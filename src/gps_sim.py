#!/usr/bin/env python

import random
import rospy
from geometry_msgs.msg import PoseWithCovarianceStamped
from gazebo_msgs.msg import ModelStates


class GpsSim(object):
    """ This node reads the position of a model in the Gazebo Model State topic and publishes it as a GPS. """

    def __init__(self, model):
        """ Constructor """
        self.model = model
        self.gps_data = PoseWithCovarianceStamped()
        self.gps_data.header.frame_id = 'world'
        self.gps_data.pose.covariance = [0.0] * 36
        self.gps_data.pose.covariance[0] = 5.0
        self.gps_data.pose.covariance[7] = 5.0
        self.offset_x = random.gauss(0, 2)
        self.offset_y = random.gauss(0, 2)

        # Publisher
        self.pub_gps = rospy.Publisher(self.model + "/gps", PoseWithCovarianceStamped, queue_size = 2)
        # Subscriber
        rospy.Subscriber("/gazebo/model_states", ModelStates, self.model_state_callback, queue_size=1)
        # Timer to publish time since init (or last time reset)
        rospy.Timer(rospy.Duration(1.0), self.publish_gps)


    def model_state_callback(self, state):
        index = state.name.index(self.model)
        if index >= 0:
            self.gps_data.pose.pose.position = state.pose[index].position

    def publish_gps(self, e):
        self.gps_data.pose.pose.position.x = self.gps_data.pose.pose.position.x + self.offset_x + random.gauss(0, 0.25)
        self.gps_data.pose.pose.position.y = self.gps_data.pose.pose.position.y + self.offset_y + random.gauss(0, 0.25)
        self.gps_data.pose.pose.position.z = self.offset_y + random.gauss(0, 0.1)
        self.gps_data.header.stamp = rospy.Time.now()
        self.pub_gps.publish(self.gps_data)

if __name__ == '__main__':
    try:
        rospy.init_node('gps_sim')
        GpsSim = GpsSim('simple_car')
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
